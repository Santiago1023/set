import java.util.*;

public class Main
{
    public static void main(String[] args) {

        Set<String> setA = new HashSet();

        setA.add("Santiago");
        setA.add("Pepe");
        setA.add("Cata");
        setA.add("Caro");
        System.out.println("Muestra solo los elementos del setA" + setA.toString());


        Set<String> setB= new HashSet();

        setB.add("hola");
        setB.add("mundo");
        setB.add("como");
        setB.add("esta");
        System.out.println("Muestra solo los elementos del setB" + "" + setB.toString() + " y el tamaño es : " + setB.size());

        setA.addAll(setB);
        System.out.println("Mostramos una combinacion de las dos :" + "" + setA.toString());

        setB.clear();
        System.out.println("Vamos a mostrar un espacio vacio porque eliminamos todos los elementos del setB " + setB.toString());
        System.out.println("¿Está vacía el setB? " + setB.isEmpty());

        List<String> listaSet = new ArrayList<>();
        listaSet.addAll(setA);
        System.out.println("Aqui vemos como pasamos los datos del setA a la nueva lista : " + listaSet.toString());
    }


}
